import random

def computerguess():
    #get computer's code guess
    for x in range(4):
        a = random.choice(['A', 'B', 'C', 'D', 'E', 'F'])
        code.append(a)
    return code

#make initalize board function - 10 rows with 4 spaces each
def initialize_board():
    board = []
    for i in range(11):
        board[i].append(" ", " ", " ", " ")
    return board

#make updated board function, with playerguess, then send board and clues variables to drawboard function
#def update_board(playerguesses, clues, turn):







def drawboard(playerguesses, clues): #switch to board parameter
    #draw the board
    hline = "+-----+-----+-----+-----+----------"

    #put in if statement to check if player guessed the correct code and print out
    for i in range(len(playerguesses)): #cycle through right amount of lines
        print(hline)
        for j in range(4):
            print("| ", playerguesses[i][j], " ", end='')
        print("| ", clues[i])
        print(hline)  #comment this out, the line is doubled up


def movevalid(guess):
    if len(guess) != 4:
        return False
    else:
        for i in range(4): #checking if it is 4 letters and between A and F
            if guess[i] not in 'ABCDEF':
                return False
        return True



def getplayerguess():
    #get player guess, make it upper case and check for valid move
    while True:
        guessStr = input("Guess the 4 letter code, using letters A - F. No spaces.")
        guess = guessStr.upper()
        #return guess #took out list converstion method
        if movevalid(guess):
            #guess = list(guessStr)
            return guess
        else:
            print("That is not a valid code. Try again.")

def checkguess(guess, code):
    #if guess == code: #do you need to check this here in the function? why not the main loop?
        #return True
    #check if letter is in exact location

    #holdguess = []
    clues =[]
    temp = code.copy()  # need to use copy method, otherwise it will change the computers code!

    holdguess = list(guess)

    # loop to check for black pegs
    for i in range(4):
        if holdguess[i] == code[i]:
            clues.append("X")  # black peg-correct letter and position
            temp[i] = "M"  # remove correct location from checking
            holdguess[i] = "Q"  # take out of guess so not counted as white peg

    # loop to check for white pegs
    for j in range(4):
        if temp[j] in holdguess:
            clues.append("O")  # white peg-correct letter, wrong position
            holdguess.remove(temp[j])  # remove from check so not counted twice

    clues.sort(reverse=True)  # sort so X's are always first so player can't tell which are correct
    clue = ''.join(clues) #convert list to a string with no spaces and return
    #print(clue)

    return clue

#main loop
playerguesses = []
code = []
clues = []

code = computerguess()
codeStr = ''.join(code) #make string version of the code with no spaces
#print(code)  only here to test
turn = 0
wongame = False


while turn < 10:
    guess = getplayerguess()
    #print(guess)
    playerguesses.append(list(guess))
    if guess == codeStr:
        wongame = True
        print("You got it!!!")
        break
    clue = checkguess(guess, code)
    clues.append(clue)
    #update board(
    #print(playerguesses, clues)
    drawboard(playerguesses, clues) #revise so only send board parameter
    turn += 1

if wongame:
    print("You guessed the code in", turn+1, "tries.")
else:
    print("The correct code is ", codeStr)

